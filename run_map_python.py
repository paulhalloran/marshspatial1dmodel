import os
import subprocess
import shutil
from math import cos, asin, sqrt


output_file_name = 'output_map2'
meterological_file_name = 'meterological_data.dat'
domain_file_name = 's12_paul_tst_h_map.dat'

#note that the filename needs to be of exactly this length (22 characters)
executable_file_name = 's2p3v7_reg_v1_var_met'


def distance(lat1, lon1, lat2, lon2):
    p = 0.017453292519943295
    a = 0.5 - cos((lat2-lat1)*p)/2 + cos(lat1*p)*cos(lat2*p) * (1-cos((lon2-lon1)*p)) / 2
    return 12742 * asin(sqrt(a))

def closest(data, v):
    return min(data, key=lambda p: distance(v['lat'],v['lon'],p['lat'],p['lon']))

def return_domain_lon(filename,i):
    f=open(filename)
    lines=f.readlines()
    return filter(lambda a: a != '', lines[i+1].split(' '))[0:2]



try:
    shutil.move('../output/'+output_file_name, '../output/'+output_file_name+'_previous')
except:
    print 'no previous output fileto move'

num_lines = sum(1 for line in open('../domain/'+domain_file_name)) - 1


for i in range(num_lines):
    print i
    lon,lat = return_domain_lon('../domain/'+domain_file_name,i)
    v = {'lat': float(lat), 'lon': float(lon)}
    print(closest(tempDataList, v))
    shutil.copyfile('../met/'+meterological_file_name+str(i),'./meterological_data.dat')
    run_model = """./"""+executable_file_name+""" << EOF >> ../output/"""+output_file_name+"""
../domain/"""+domain_file_name+"""
map
"""+str(i+1)+"""
../domain/"""+domain_file_name+"""
map
"""+str(i+1)+"""
EOF"""
    subprocess.call(run_model, shell=True)
