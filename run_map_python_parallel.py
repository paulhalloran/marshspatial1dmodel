import os
import subprocess
import shutil
import glob
from math import cos, asin, sqrt
import multiprocessing as mp
from functools import partial

output_file_name = 'output_map3'
meterological_file_name = 'meterological_data'
domain_file_name = 's11_11_11_11_h_map.dat'
base_directory = '/gpfs/ts0/home/ph290/marsh_model/s2p3_reg_v1.0/'
#note that the filename needs to be of exactly this length (22 characters)
executable_file_name = 's2p3v7_reg_v1_var_met_parallel'

met_data_location = base_directory+'met/spatial_data/'

def distance(lat1, lon1, lat2, lon2):
    p = 0.017453292519943295
    a = 0.5 - cos((lat2-lat1)*p)/2 + cos(lat1*p)*cos(lat2*p) * (1-cos((lon2-lon1)*p)) / 2
    return 12742 * asin(sqrt(a))

def closest(data, lat1,lon1):
    return min(data, key=lambda p: distance(lat1,lon1,p[0],p[1]))

def return_domain_lon(filename,i):
    f=open(filename)
    lines=f.readlines()
    return filter(lambda a: a != '', lines[i+1].split(' '))[0:2]



try:
    shutil.move(base_directory+'output/'+output_file_name, base_directory+'output/'+output_file_name+'_previous')
except:
    print 'no previous output fileto move'

num_lines = sum(1 for line in open(base_directory+'domain/'+domain_file_name)) - 1


files = glob.glob(met_data_location+'*.dat')
w, h = 2, len(files) ;
lats_lons = [[0 for x in range(w)] for y in range(h)]
for i,file in enumerate(files):
    tmp = file.split('lat')[-1].split('.dat')[0].split('lon')
    lats_lons[i][0] = float(tmp[0])
    lats_lons[i][1] = float(tmp[1])




def run_model(domain_file_name,lats_lons,i):
    #modifying so that the fortran code looks for the correct met file, rather than us having to copy it into the working dorectory
    lon,lat = return_domain_lon(base_directory+'domain/'+domain_file_name,i)
    forcing_lat_lon = closest(lats_lons, float(lat),float(lon))
    # shutil.copyfile(met_data_location+meterological_file_name+'lat'+str(forcing_lat_lon[0])+'lon'+str(forcing_lat_lon[1])+'.dat','./meterological_data.dat')
    run_command = """./"""+executable_file_name+""" << EOF
"""+str(forcing_lat_lon[0])+"""
"""+str(forcing_lat_lon[1])+"""
../domain/"""+domain_file_name+"""
map
"""+str(i+1)+"""
"""+str(forcing_lat_lon[0])+"""
"""+str(forcing_lat_lon[1])+"""
../domain/"""+domain_file_name+"""
map
"""+str(i+1)+"""
EOF"""
    proc = subprocess.Popen([run_command],  shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    out, err = proc.communicate()
    return out



# import time
# t0 = time.time()
pool = mp.Pool(processes=16)
func = partial(run_model, domain_file_name, lats_lons)
results = pool.map(func, range(num_lines))
# t1 = time.time()
# print t1-t0

with open(base_directory+'output/'+output_file_name,'w') as fout:
    for result in results:
        fout.write(result)
